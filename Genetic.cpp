#include <windows.h>
#include "Entete.h"

#include "Constraints.h"
#include "ValidationRules.h"

#pragma comment (lib,"GeneticDLL.lib")

//%%%%%%%%%%%%%%%%%%%%%%%%% IMPORTANT: %%%%%%%%%%%%%%%%%%%%%%%%% 
//Le fichier de probleme (.dat) doit se trouver dans le r�pertoire courant du projet pour une ex�cution � l'aide du compilateur.
//Les fichiers de la DLL (GeneticDLL.dll et GeneticDLL.lib) doivent se trouver dans le m�me r�pertoire que l'ex�cutable (.exe-DEBUG) et 
//dans le r�pertoire courant du projet pour une ex�cution � l'aide du compilateur.
//Indiquer les arguments du programme dans les propri�t�s du projet - d�bogage - arguements.
//Sinon, utiliser le r�pertoire execution.

//*****************************************************************************************
// Prototype des fonctions se trouvant dans la DLL 
//*****************************************************************************************
//DESCRIPTION:	Lecture du Fichier probleme et initialiation de la structure Problem
extern "C" _declspec(dllimport) void LectureProbleme(std::string FileName, TProblem & unProb, TGenetic &unGenetic);

//DESCRIPTION:	Fonction d'affichage � l'�cran permettant de voir si les donn�es du fichier probl�me ont �t� lues correctement
extern "C" _declspec(dllimport) void AfficherProbleme (TProblem unProb);

//DESCRIPTION:	�valuation de la fonction objectif d'une solution et MAJ du compteur d'�valuation. 
//				Retourne la solution modifi�e avec retard total pond�r� et validit� de la solution (respect contraintes pr�c�dence)
extern "C" _declspec(dllimport) void EvaluerSolution(TIndividu & uneSol, TProblem unProb, TGenetic &unGenetic);

//DESCRIPTION: Fonction qui g�n�re une population initiale en s'assurant d'avoir des solutions valides*/
extern "C" _declspec(dllimport) void CreerPopInitialeAleaValide(std::vector<TIndividu> & unePop, TProblem unProb, TGenetic & unGenetic);

//DESCRIPTION: Fonction qui affiche le d�tail des solutions (de Debut jusqu'a Fin-1) dans la population courante
extern "C" _declspec(dllimport) void AfficherSolutions(std::vector<TIndividu> unePop, int Debut, int Fin, int Iter, TProblem unProb, bool AvecCalcul);

//DESCRIPTION: Fonction de tri croissant des individus dans la population entre Debut et Fin-1 INCLUSIVEMENT 
extern "C" _declspec(dllimport) void TrierPopulation(std::vector<TIndividu> & unePop, int Debut, int Fin);

//DESCRIPTION: Copie de la s�quence et de la fonction objectif dans une nouvelle TSolution. La nouvelle TSolution est retourn�e.
extern "C" _declspec(dllimport) void CopierSolution (const TIndividu uneSol, TIndividu &Copie, TProblem unProb);

//DESCRIPTION: Fonction qui r�alise la MUTATION (modification al�atoire) sur une solution: �change de 2 commandes adjacentes qui respecte les contraintes de pr�c�dence.
//La solution est ensuite �valu�e et v�rifi�e. 
extern "C" _declspec(dllimport) void Mutation(TIndividu & Mutant, TProblem unProb, TGenetic & unGen);

//DESCRIPTION: Fonction de s�lection d'un individu par tournoi
extern "C" _declspec(dllexport) int Selection (std::vector<TIndividu> unePop, int _Taille, TProblem unProb);

//DESCRIPTION: Fonction affichant les r�sultats de l'algorithme g�n�tique
extern "C" _declspec(dllexport) void AfficherResultats (TIndividu uneBest, TProblem unProb, TGenetic unGen);

//DESCRIPTION: Fonction affichant les r�sultats de l'algorithme g�n�tique dans un fichier texte
extern "C" _declspec(dllexport) void AfficherResultatsFichier (TIndividu uneBest, TProblem unProb, TGenetic unGen, std::string FileName);

//DESCRIPTION:	Lib�ration de la m�moire allou�e dynamiquement
extern "C" _declspec(dllexport) void LibererMemoireFinPgm (std::vector<TIndividu> & unePop, std::vector<TIndividu> & unePopEnfant, TIndividu & uneBest, TProblem & unProb, TGenetic unGen);

//*****************************************************************************************
// Prototype des fonctions locales 
//*****************************************************************************************

// Main methods
void runMultipleTimes(const int &loops, const CrossoverType &crossoverType, vector<TIndividu> &population, vector<TIndividu> &children, TIndividu &best, TProblem &instance, TGenetic &genetic);
void invokeGeneticAlgorithm(const CrossoverType &crossoverType, vector<TIndividu> &population, vector<TIndividu> &children, TIndividu &best, const TProblem &instance, TGenetic &genetic);
void initialEnvironmentSetup(vector<TIndividu> &population, TIndividu &best, TProblem &instance, TGenetic &genetic);
void saveResults(const CrossoverType &crossoverType, TIndividu &best, const TProblem &instance, TGenetic &genetic);

// DNA Corruption (?)
int findValidParent(const vector<TIndividu> &population, const TGenetic &genetic, const TProblem &instance);
bool validDNASequence(const TIndividu &individual);
void assertIntegrity(const TIndividu &p);

// Crossover related methods
TIndividu Croisement(const TIndividu &parent1, const TIndividu &parent2, const TProblem &instance, TGenetic &genetic, const CrossoverType &crossoverType);
vector<int> createOffspring(const TIndividu &parent1, const TIndividu &parent2, const CrossoverType &crossoverType);
vector<int> processModifiedCrossover(const TIndividu &parent1, const TIndividu &parent2, const unsigned int &sequenceSize);
vector<int> processOrderCrossover(const TIndividu &parent1, const TIndividu &parent2, const unsigned int &sequenceSize);
vector<int> processCustomCrossover(const TIndividu &parent1, const TIndividu &parent2, const unsigned int &sequenceSize);
void fixOffspring(TIndividu &offspring, const TProblem &unProb, TGenetic &unGen);


// Replacement related methods
void Remplacement(vector<TIndividu> &parents, const vector<TIndividu> &children, const TProblem &instance, const TGenetic &unGen, const int tournamentSize);
void removeIndividualFrom(const TIndividu &winner, vector<TIndividu> &population);

// Mutation related methods
bool mutationTriggered(const TGenetic &genetic);

// Utils
void displayProgress(TGenetic &genetic);
void flushDisplay();
int generateRandomNumberBetween(const int &lowerBound, const int &upperBound);
vector<int> generateRandomPositions(const int &expectedSize, const int &upperBound);
bool vectorContains(const vector<int> &haystack, const int &needle);

//******************************************************************************************
// INLINE OPERATORS
//*****************************************************************************************

inline bool operator==(const TIndividu &lhs, const TIndividu &rhs) {
	bool sameSequence = true;
	for(unsigned int i=0; i<lhs.Seq.size(); i++) {
		sameSequence = sameSequence && lhs.Seq[i] == rhs.Seq[i];
	}
	return sameSequence;
}

std::ostream& operator<<(std::ostream& lhs, const CrossoverType &crossoverType) {
    switch(crossoverType) {
		
		case CUSTOM_CROSSOVER: 
			lhs << "CUSTOM CROSSOVER"; 
			break;

		case ORDER_CROSSOVER: 
			lhs << "ORDER CROSSOVER (OX)"; 
			break;

		default:
		case MODIFIED_CROSSOVER: 
			lhs << "MODIFIED CROSSOVER"; 
			break;
    }
    return lhs;
}

//******************************************************************************************
// Fonction main
//*****************************************************************************************
int main(int NbParam, char *Param[])
{
	string filename;
	TProblem instance;				//**D�finition de l'instance de probl�me
	TGenetic genetic;				//**D�finition des param�tres de l'algorithme genetique
	vector<TIndividu> population;	//**Population compos�e de Taille_Pop Individus 
	vector<TIndividu> children;		//**Population d'enfant
	TIndividu best;					//**Meilleure solution depuis le d�but de l'algorithme
	
	//**Lecture des param�tres
	filename.assign(Param[1]);
	genetic.TaillePop	= atoi(Param[2]);
	genetic.ProbCr		= atof(Param[3]);
	genetic.ProbMut		= atof(Param[4]);
	genetic.NB_EVAL_MAX	= atoi(Param[5]);
	genetic.TaillePopEnfant	= (int)ceil(genetic.ProbCr * genetic.TaillePop);
	genetic.Gen = 0;

	//**D�finition de la dimension des tableaux
	population.resize(genetic.TaillePop);		//**Le tableau utilise les indices de 0 � TaillePop-1.
	children.resize(genetic.TaillePopEnfant);	//**Le tableau utilise les indices de 0 � TaillePopEnfant-1

	//**Lecture du fichier de donnees
	LectureProbleme(filename, instance, genetic);

	runMultipleTimes(10, MODIFIED_CROSSOVER, population, children, best, instance, genetic);
	runMultipleTimes(10, ORDER_CROSSOVER, population, children, best, instance, genetic);
	runMultipleTimes(10, CUSTOM_CROSSOVER, population, children, best, instance, genetic);

	LibererMemoireFinPgm(population, children, best, instance, genetic);

	system("PAUSE");
	return 0;
}

void runMultipleTimes(const int &loops, const CrossoverType &crossoverType, vector<TIndividu> &population, vector<TIndividu> &children, TIndividu &best, TProblem &instance, TGenetic &genetic) {
	for(int i=0; i<loops; i++) {
		cout << endl << "	- [LOOP #" << i+1 << "] Crossover selected: " << crossoverType << endl;
		initialEnvironmentSetup(population, best, instance, genetic);
		invokeGeneticAlgorithm(crossoverType, population, children, best, instance, genetic);
	}
}

void initialEnvironmentSetup(vector<TIndividu> &population, TIndividu &best, TProblem &instance, TGenetic &genetic) {
	// Reset of evaluation count
	genetic.CptEval = 0;
	genetic.Gen = 0;
	
	//**Initialisation et trie de la population initiale NB: Initialisation de la population entraine des evaluation de solutions.
	//**CptEval est donc = TaillePop au retour de la fonction.
	CreerPopInitialeAleaValide(population, instance, genetic);
	TrierPopulation(population, 0, genetic.TaillePop);
	
	//**Initialisation de de la meilleure solution
	CopierSolution(population[0], best, instance);
	cout << "	- [INIT] Random initial solution scored: " << best.FctObj << endl << endl;  //**NE PAS ENLEVER
}

void invokeGeneticAlgorithm(const CrossoverType &crossoverType, vector<TIndividu> &population, vector<TIndividu> &children, TIndividu &best, const TProblem &instance, TGenetic &genetic) {
	int parent1, parent2;    //**Indices de solution des parents

	//**Boucle principale de l'algorithme g�n�tique
	do 
	{
		displayProgress(genetic);

		//**S�lection et croisement
		for (int i=0; i<genetic.TaillePopEnfant; i++)
		{
			//**S�LECTION de deux parents
			parent1 = findValidParent(population, genetic, instance);
			parent2 = findValidParent(population, genetic, instance);
			
			//**CROISEMENT entre les deux parents. Cr�ation d'UN enfant.
			children[i] = Croisement(population[parent1], population[parent2], instance, genetic, crossoverType);

			//**MUTATION d'une solution
			if (mutationTriggered(genetic)) {
				//V�rification pour ne pas perdre la meilleure solution connue avant mutation
				if (best.FctObj > children[i].FctObj) {
					CopierSolution(children[i], best, instance);
				}
				Mutation(children[i], instance, genetic);
			}
		}
		
		//**REMPLACEMENT de la population pour la prochaine g�n�ration
		Remplacement(population, children, instance, genetic, 3);

		//**Conservation de la meilleure solution
		TrierPopulation(population, 0, genetic.TaillePop);
		if (best.FctObj > population[0].FctObj) {				//**NE PAS ENLEVER
			CopierSolution(population[0], best, instance);
			cout << endl << "		Improvement of current best: " << best.FctObj << endl << endl;
		}

	} while (genetic.CptEval < genetic.NB_EVAL_MAX);	//**NE PAS ENLEVER

	flushDisplay();
	saveResults(crossoverType, best, instance, genetic);
}

int findValidParent(const vector<TIndividu> &population, const TGenetic &genetic, const TProblem &instance) {
	int parent;
	do {
		parent = Selection(population, genetic.TaillePop, instance);
	} while(!validDNASequence(population[parent]) || !population[parent].Valide);
	return parent;
}

bool validDNASequence(const TIndividu &individual) {
	vector<int> occurences(individual.Seq.size(), 0);
	for(unsigned int i=0; i<individual.Seq.size(); i++) {
		occurences[individual.Seq[i]]++;
		if(occurences[individual.Seq[i]] > 1) {
			return false;
		}
	}
	return true;
}

/**
 * Returns wether the offspring should get mutated or not
 */
bool mutationTriggered(const TGenetic &genetic) {
	double random = double(rand()) / double(RAND_MAX);
	return random < genetic.ProbMut;
}

void saveResults(const CrossoverType &crossoverType, TIndividu &best, const TProblem &instance, TGenetic &genetic) {
	AfficherResultats (best, instance, genetic);		//**NE PAS ENLEVER
	
	switch(crossoverType) {
		case ORDER_CROSSOVER:
			AfficherResultatsFichier (best, instance, genetic, "Resutats-ORDER-CROSSOVER.txt");
			break;

		case CUSTOM_CROSSOVER:
			AfficherResultatsFichier (best, instance, genetic, "Resutats-CUSTOM-CROSSOVER.txt");
			break;

		case MODIFIED_CROSSOVER:
		default:
			AfficherResultatsFichier (best, instance, genetic, "Resutats-MODIFIED-CROSSOVER.txt");
			break;
	}
}

//******************************************************************************************************
//**Fonction qui r�alise le CROISEMENT (�change de genes) entre deux parents. Retourne l'enfant produit.
//******************************************************************************************************
//**A D�FINIR PAR L'�TUDIANT****************************************************************************
//**NB: IL FAUT RESPECTER LA DEFINITION DES PARAM�TRES AINSI QUE LE RETOUR DE LA FONCTION
//****************************************************************************************************** 
TIndividu Croisement(const TIndividu &parent1, const TIndividu &parent2, const TProblem &instance, TGenetic &genetic, const CrossoverType &crossoverType)
{	
	TIndividu offspring;
	CopierSolution(parent1, offspring, instance);

	offspring.Seq = createOffspring(parent1, parent2, crossoverType);
	
	//**NE PAS ENLEVER
	EvaluerSolution(offspring, instance, genetic);

	if(!offspring.Valide) {  
		fixOffspring(offspring, instance, genetic);
	}

	return offspring;
}

/**
 * Class used to define constraints concerning one specific predecessor and all its sucessors.
 */
vector<int> createOffspring(const TIndividu &parent1, const TIndividu &parent2, const CrossoverType &crossoverType) {
	const unsigned int sequenceSize = parent1.Seq.size();

	switch(crossoverType) {
		case ORDER_CROSSOVER:
			return processOrderCrossover(parent1, parent2, sequenceSize);

		case CUSTOM_CROSSOVER:
			return processCustomCrossover(parent1, parent2, sequenceSize);

		case MODIFIED_CROSSOVER:
		default:
			return processModifiedCrossover(parent1, parent2, sequenceSize);
	}
}

/**
 * Modified crossover (Davis)
 * L. Davis, Applying adaptive algorithms to epistactic domains, in: Proc. Int. Joint Conf. on Artificial Intelligence (IJCAI '85), Los Angeles, CA (1985) pp. 162-164.
 */
vector<int> processModifiedCrossover(const TIndividu &parent1, const TIndividu &parent2, const unsigned int &sequenceSize) {
	
	// Copy the first elements from parent1 up until the cut position
	int pivot = generateRandomNumberBetween(1, parent1.Seq.size()-1);
	vector<int> sequence (pivot);
	copy(parent1.Seq.begin(), parent1.Seq.begin() + pivot, sequence.begin());

	// Then add elements from parent2 only if they not already exist in offspring's DNA sequence (starting from the start)
	for (unsigned int i=0; i<sequenceSize; i++) {
		if (find(sequence.begin(), sequence.end(), parent2.Seq.at(i)) == sequence.end()) {
			sequence.push_back(parent2.Seq.at(i));
		}
	}

	return sequence;
}

/**
 * Custom crossover developped over the Modified crossover (Davis), see @processModifiedCrossover 
 * The main difference is where the examination of parent2 is started > in this crossover, here we examinate parent2 from the cut-position+1 (instead of the start of parent2's DNA sequence)
 */
vector<int> processCustomCrossover(const TIndividu &parent1, const TIndividu &parent2, const unsigned int &sequenceSize) {
	
	// Copy the first elements from parent1 up until the cut position
	int pivot = generateRandomNumberBetween(1, parent1.Seq.size()-1);
	vector<int> sequence (pivot);
	copy(parent1.Seq.begin(), parent1.Seq.begin() + pivot, sequence.begin());

	// Then add elements from parent2 only if they not already exist in offspring's DNA sequence (starting from cut position + 1)
	//int explorationIndex = (pivot) % sequenceSize;
	int explorationIndex = pivot % sequenceSize;
	while(sequence.size() != sequenceSize) {
		if (find(sequence.begin(), sequence.end(), parent2.Seq.at(explorationIndex)) == sequence.end()) {
			sequence.push_back(parent2.Seq.at(explorationIndex));
		}
		explorationIndex = (explorationIndex + 1) % sequenceSize;
	}
	
	return sequence;
}

/**
 * Order Crossover (OX) - Oliver et al. [1], Goldberg [2]
 * [1] I.M. Oliver, D.J. Smith and J.R.C. Holland, A study of permutation crossover operators on the traveling salesman problem, in: Proc. 2nd Int. Conf. on Genetic Algorithms ( ICGA '87), Massachusetts Institute of Technology, Cambridge, MA (1987) pp. 224-230.
 * [2] D.E. Goldberg, Genetic Algorithms in Search, Optimization and Machine Learning (Addison-Wesley, 1989).
 */
vector<int> processOrderCrossover(const TIndividu &parent1, const TIndividu &parent2, const unsigned int &sequenceSize) {

	// Pick 2 cut points at random (with a minimum distance of 1 between the two)
	const int firstCutPoint = generateRandomNumberBetween(1, parent1.Seq.size()-2);
	const int secondCutPoint = generateRandomNumberBetween(firstCutPoint+1, parent1.Seq.size()-1);
	
	// Copy elements from parent1 from cutPoint01 up until cutPoint02, initializing all other values to -1
	vector<int> sequence(sequenceSize, -1);
	copy(parent1.Seq.begin() + firstCutPoint, parent1.Seq.begin() + secondCutPoint, sequence.begin() + firstCutPoint);

	// Initialization of starting positions (for both exploration of parent2, and insertion in the offspring's DNA sequence
	int insertionIndex = (secondCutPoint) % sequenceSize;
	int explorationIndex = (secondCutPoint) % sequenceSize;

	// Then add elements from parent2 only if they not already exist in offspring's DNA sequence (starting from secondCutPoint)
	// currentSize is used to keep track of many elements has been successfully set
	int currentSize = secondCutPoint - firstCutPoint;
	unsigned int cycleCount = 0;
	while(currentSize != sequenceSize) {
		if (find(sequence.begin(), sequence.end(), parent2.Seq.at(explorationIndex)) == sequence.end()) {
			sequence[insertionIndex] = parent2.Seq.at(explorationIndex);
			insertionIndex = (insertionIndex + 1) % sequenceSize;
			currentSize++;
		}
		explorationIndex = (explorationIndex + 1) % sequenceSize;
	}
	return sequence;
}

void fixOffspring(TIndividu &offspring, const TProblem &instance, TGenetic &genetic) {

	TIndividu backup, backup2;
	CopierSolution(offspring, backup, instance);

	// Retrieve all validation rules and fix offspring's DNA sequence
	ValidationRules& rules = ValidationRules::getValidationRules(instance);
	rules.fixOffspring(offspring);
	CopierSolution(offspring, backup2, instance);

	// Re-evaluate fixed offspring
	EvaluerSolution(offspring, instance, genetic);

	// If ffspring's DNA sequence is still invalid, re-run fixing process one last time
	if(!offspring.Valide) {
		rules.fixOffspringBackwards(offspring);
		EvaluerSolution(offspring, instance, genetic);
		if(!offspring.Valide) { 
			cout <<endl << "		WARNING > Offspring fixing failed to produce a valid DNA sequence" << endl;
		}
	}
}

//*******************************************************************************************************
//Fonction qui r�alise le REMPLACEMENT de la population pour la prochaine g�n�ration. Cette fonction doit
//prendre les TaillePop solutions de la population "Parents" et les TaillePopEnfant solutions de la 
//population "Enfants" et retenir SEULEMENT TaillePop solutions pour commencer la prochaine g�n�ration. 
//Les TaillePop solutions retenues doivent �tre plac�es dans la populations "Parents".  
//*******************************************************************************************************
//**A D�FINIR PAR L'�TUDIANT*****************************************************************************
//**NB: IL FAUT RESPECTER LA DEFINITION DES PARAM�TRES
//******************************************************************************************************* 
void Remplacement(vector<TIndividu> &parents, const vector<TIndividu> &children, const TProblem &instance, const TGenetic &unGen, const int tournamentSize)
{
	// Instanciation of a vector containing the complete population (merging both parents and children populations)
	vector<TIndividu> completePopulation;
	completePopulation.reserve(parents.size() + children.size());
	completePopulation.insert(completePopulation.end(), parents.begin(), parents.end());
	completePopulation.insert(completePopulation.end(), children.begin(), children.end());

	// Clear parents population
	parents.clear();

	// While population size hasn't been restored
	while(parents.size() != unGen.TaillePop) {
		
		// Populate tournament contestants at random
		vector<TIndividu> tournament;
		unsigned int contestants = 0;
		while(tournament.size() != tournamentSize) {
			int random = rand() % completePopulation.size();
			if(completePopulation[random].Valide) {
				tournament.push_back(completePopulation[random]);
			}
		}

		// Add the winner in parent population, remove it from the complete population
		TrierPopulation(tournament, 0, tournament.size());
		parents.push_back(tournament[0]);
		removeIndividualFrom(tournament[0], completePopulation);
		tournament.clear();
	}
}

/**
 * Remove specified TInvidu from provided population.
 * Used to make sure the same parent isn't added multiple times into the resulting parent population
 */
void removeIndividualFrom(const TIndividu &winner, vector<TIndividu> &population) {
	population.erase(std::remove(population.begin(), population.end(), winner), population.end());
}

/*===========================================================*/
/*                           UTILS                           */
/*===========================================================*/

/**
 * Generate a random number in range of [lowerBound - uppberBound] both included.
 */
int generateRandomNumberBetween(const int &lowerBound, const int &upperBound) {
	return rand() % (upperBound - lowerBound + 1) + lowerBound;;
}

vector<int> generateRandomPositions(const int &expectedSize, const int &upperBound) {
	vector<int> randomPositions;

	while(randomPositions.size() != expectedSize) {
		int random;
		do {
			random = rand() % upperBound;
		} while (vectorContains(randomPositions, random));
		randomPositions.push_back(random);
	}

	cout << "Random positions:";
	for(int i=0; i<expectedSize; i++) {
		cout << randomPositions[i] << " ";
	}
	cout << endl;
	return randomPositions;
}

/**
 * Update display to display progress (both generatio number and remaining evaluations).
 */
void displayProgress(TGenetic &genetic) {
	cout << "\r	- [PROGRESS] Generation " << genetic.Gen++ << " (Evaluations remaining: " << genetic.NB_EVAL_MAX - genetic.CptEval << ")      " << flush; 
}

void flushDisplay() {
	cout << endl << endl;
}

bool vectorContains(const vector<int> &haystack, const int &needle) {
	return (find(haystack.begin(), haystack.end(), needle) != haystack.end());
}

void assertIntegrity(const TIndividu &p) {
	vector<int> occurences(p.Seq.size(), 0);
	for(unsigned int i=0; i<p.Seq.size(); i++) {
		occurences[p.Seq[i]]++;
		assert(occurences[i] <= 1);
	}
}