#include "Constraints.h"

/*===========================================================*/
/*                        CONSTRUCTOR                        */
/*===========================================================*/

Constraints::Constraints(const int predecessor) {
	this->predecessor = predecessor;
}

const void Constraints::addSucessor(const int successor) {
	this->successors.push_back(successor);
}

/*============================================================*/
/*                   PREDECESSOR & SUCCESSORS                 */
/*============================================================*/

/**
 * Retuns a constant reference of given predecessor associated with this constraint.
 */
const int& Constraints::getPredecessor() const {
	return this->predecessor;
}

/**
 * Returns a constant reference of given list of successors associated with this constraint.
 */
const vector<int>& Constraints::getSuccessors() const {
	return this->successors;
}

/**
 * Returns the number of sucessors associated with this constraint.
 */
const size_t Constraints::getSuccessorsCount() const {
	return this->successors.size();
}

/*==========================================================*/
/*                         ORDERING                         */
/*==========================================================*/

/**
 * Operator overload allowing constraints to be sorted depending on their number of sucessors.
 */
bool operator<(const Constraints& cs1, const Constraints& cs2) {
	return cs1.getSuccessorsCount() > cs2.getSuccessorsCount();
}
