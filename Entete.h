#ifndef __ENTETE_H_
#define __ENTETE_H_

#include <cstdio>
#include <cstdlib> 
#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <ctime>  
#include <cmath>
#include <vector>

#include <assert.h>
#include <chrono>

using namespace std;

struct TProblem								//**D�finition du probl�me:
{
	std::string Nom;						//**Nom du fichier de donn�es
	int NbCom;								//**Taille du probleme: Nombre de commandes
	std::vector<int> Temps;					//**Temps de production pour fabriquer chaque commande. NB: Tableau de 0 � NbCom-1.  
	std::vector<int> DateDue;				//**Date due de chaque commande. NB: Tableau de 0 � NbCom-1.
	std::vector<int> Poids;					//**Poids (importance) de chaque commande. NB: Tableau de 0 � NbCom-1.
	std::vector<std::vector <int> > Pred;	//**Pour chaque commande: Liste des commandes qui doivent d'abord �tre r�alis�es. 
											// NB: 2e dimension variable selon le nombre de pr�d�cesseurs.
	std::vector<std::vector <int> > Succ;	//**Pour chaque commande: Liste des commandes qui doivent �tre r�alis�es apr�s (successeur). 
											//NB: 2e dimension variable selon le nombre de successeurs.
	std::vector<std::vector <bool> > MatPred;	//**Matrice de bool�en entre chaque paire de commandes indiquant si la commande en ligne est pr�d�cesseur
												//� la commande en colonne. NB: Tableau de 0 � NbCom-1.  Indice 0 utilis�.
};

struct TIndividu						//**D�finition d'une solution: 
{
	std::vector<int> Seq;				//**Indique la s�quence de production des commandes. NB: Tableau de 0 � NbCom-1.
	bool Valide;						//**Indique si la solution respecte les contraintes de pr�c�dence. V�rifier lorsque la solution est �valu�e.
	long FctObj;						//**Valeur de la fonction obj: Sommation des retards pond�r�s
	std::vector<int> Fin;				//**Temps de terminaison d'une commande dans la s�quence
	std::vector<int> Retard;			//**Indique le nombre d'unit�s de temps qu'une commande est produite en retard: Max {0, Fin-DateDue}
	std::vector<int> RetardP;			//**Calcul le retard pond�r� pour une commande (Retard*Poids)	
};

struct TGenetic
{
	int		TaillePop;					//**Taille de la population (nombre d'individus)

	int		TaillePopEnfant;			//**Taille de la populationEnfant (nombre d'enfants)
	double	ProbCr;						//**Probabilit� de croisement [0%,100%]
	double	ProbMut;					//**Probabilit� de mutation [0%,100%] 
	int		Gen;						//**Compteur du nombre de g�n�rations

	int		CptEval;					//**COMPTEUR DU NOMBRE DE SOLUTIONS EVALUEES. SERT POUR CRITERE D'ARRET.
	int		NB_EVAL_MAX;				//**CRITERE D'ARRET: MAXIMUM "NB_EVAL_MAX" EVALUATIONS.
};

enum CrossoverType
{
	MODIFIED_CROSSOVER,
	ORDER_CROSSOVER,
	CUSTOM_CROSSOVER
};

#endif